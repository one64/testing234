hdmi="VGA-0"
dp="eDP"
connected_monitors=$(bspc query -M | wc -l)

if [ "$connected_monitors" -eq "2" ]; then
    for i in {6..10}; do bspc desktop "$i" -m "$hdmi"; done
elif [ "$connected_monitors" -eq "3" ]; then
    for i in {5..7}; do bspc desktop "$i" -m "$dp"; done
    for i in {8..10}; do bspc desktop "$i" -m "$hdmi"; done
else
    bspc monitor -d 1 2 3 4 5 6 7 8 9 10
fi

while [ "$(bspc query -D --names | grep -c 'Desktop')" -gt 0 ]; do
    bspc desktop "Desktop" -r
done