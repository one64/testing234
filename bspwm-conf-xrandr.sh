#!/bin/bash

connected_monitors="$(xrandr --query | grep -cw 'connected')"

if [ "$connected_monitors" -eq "2" ]; then
    xrandr --output eDP --auto --pos 1440x0 --output VGA-0 --auto --pos 0x0
elif [ "$connected_monitors" -eq "3" ]; then
    xrandr --output eDP --auto --pos 1440x0 --output DP1 --auto --pos 0x0 --output VGA-0 --auto --pos 2880x0 
fi